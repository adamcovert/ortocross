$(document).ready(function () {

  svg4everybody();

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
  });

  $(window).on('scroll', function () {
    if ($(window).scrollTop() > 0) {
      $('.s-page-header').addClass('s-page-header--compact');
    } else {
      $('.s-page-header').removeClass('s-page-header--compact');
    }
  });

  var teamSwiper = new Swiper('.s-team__slider .swiper-container', {
    spaceBetween: 20,
    scrollbar: {
      el: '.s-team__scrollbar',
      draggable: true,
    },
    navigation: {
      prevEl: '.s-team__slider-navigation .s-slider-navigation__btn--prev',
      nextEl: '.s-team__slider-navigation .s-slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 3
      },
      992: {
        slidesPerView: 4
      }
    }
  });
});